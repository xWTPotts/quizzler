//
//  Question.swift
//  Quizzler-iOS13
//
//  Created by Taylor Potts on 2020-06-21.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct Question {
    let question: String
    let answer: String
    
    init(q: String, a: String) {
        self.question = q
        self.answer = a
    }
}
